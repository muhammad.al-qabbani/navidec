# ansible/deploy_land.yml

# ========================
# IRISA Platform License
# ========================

# Copyright (c) [2024] [IRISA, Université de Rennes]

# This file is part of IRISA Platform.

# IRISA Platform is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, Version 3 of the License.

# IRISA Platform is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with IRISA Platform. If not, see <https://www.gnu.org/licenses/>.

---
- hosts: all
  any_errors_fatal: true
  gather_facts: no
  vars:
      ansible_host: "{{ lookup('env', 'HOST_IP_LAND') }}"
      ansible_ssh_user: "{{ lookup('env','LAND_SSH_USER') }}"
      ansible_ssh_pass: "{{ lookup('env','LAND_SSH_PASS') }}"
      ansible_ssh_extra_args: '-o StrictHostKeyChecking=no'

      HOST_IP_BOAT: "{{ lookup('env', 'HOST_IP_BOAT') }}"
      HOST_IP_LAND: "{{ lookup('env', 'HOST_IP_LAND') }}"
      PIXALARM_API_URL: "{{ lookup('env', 'PIXALARM_API_URL') }}"
      LAND_SSH_USER: "{{ lookup('env','LAND_SSH_USER') }}"
      LAND_SSH_PASS: "{{ lookup('env','LAND_SSH_PASS') }}"
      HOST_IP_RPC: "{{ lookup('env', 'HOST_IP_RPC') }}"
      MODE: "{{ lookup('env', 'MODE') }}"

      openvpn_admin_username: 'openvpn'
      openvpn_admin_password: 'IRISA@2030!'
      openvpn_user1_username: 'work'
      openvpn_user1_password: 'IRISA@2030!'
      openvpn_user1_ip: '10.10.10.10'
      openvpn_user2_username: 'works'
      openvpn_user2_password: 'IRISA@2030!'
      openvpn_user2_ip: '10.10.10.20'
  tasks:
      - name: Test connectivity
        block:
            - name: Ping all hosts (Start mode)
              ping:
              register: result
              ignore_errors: false

            - name: Fail if any host is unreachable
              fail:
                  msg: 'Host is unreachable. Exiting because not all hosts are reachable.'
              when: result is failed

            - name: Gathering Facts
              setup:
              ignore_errors: false
        tags: reload, stop_all

      - name: Setup Current Directory and Destination Directory
        block:
            - name: Get Current Directory
              command: pwd
              register: current_dir_output
              changed_when: false
              ignore_errors: true

            - name: Debug Current Directory
              debug:
                  var: current_dir_output.stdout

            - name: Set Destination Directory
              set_fact:
                  dest_directory: '{{ current_dir_output.stdout }}/navidec'
              changed_when: false

            - name: Debug Destination Directory
              debug:
                  var: dest_directory
        tags: reload, stop_all

      - name: Prepare navidec directory
        block:
            - name: Delete old navidec directory
              file:
                  path: '{{ dest_directory }}'
                  state: absent
              become: yes
              ignore_errors: false

            - name: Clone navidec repository for localhost boat
              git:
                  repo: 'https://gitlab.inria.fr/muhammad.al-qabbani/navidec.git'
                  dest: '{{ current_dir_output.stdout }}/navidec'
                  version: '{{ boat_branch }}'
                  depth: 1
                  force: yes
                  update: yes
              when: "'boat_group' in group_names"

            - name: Clone navidec repository for land
              git:
                  repo: 'https://gitlab.inria.fr/muhammad.al-qabbani/navidec.git'
                  dest: '{{ current_dir_output.stdout }}/navidec'
                  version: '{{ land_branch }}'
                  depth: 1
                  force: yes
                  update: yes
              when: "'land_group' in group_names"
        tags: reload
      # =========================== .env ==================================
      - name: extract infos to .env files
        block:
            - name: Remove existing .env file
              ansible.builtin.file:
                  path: '{{ dest_directory }}/.env'
                  state: absent
              delegate_to: boat
              run_once: true
              ignore_errors: true

            - name: Write hostname and IP address to multiple .env files for boat
              copy:
                  dest: '{{ item }}'
                  content: |
                      HOST_IP_BOAT={{ lookup('env', 'HOST_IP_BOAT') }}
                      HOST_IP_LAND={{ lookup('env', 'HOST_IP_LAND') }}
                      PIXALARM_API_URL={{ lookup('env', 'PIXALARM_API_URL') }}
                      LAND_SSH_USER={{ lookup('env','LAND_SSH_USER') }}
                      LAND_SSH_PASS={{ lookup('env','LAND_SSH_PASS') }}
                      HOST_IP_RPC={{ lookup('env', 'HOST_IP_RPC') }}
                      MODE="{{ lookup('env', 'MODE') }}"
                  force: yes
                  mode: '0644'
              loop:
                  - '{{ dest_directory }}/.env'
                  - '{{ dest_directory }}/xterm.js/.env'
                  - '{{ dest_directory }}/bandwidthvisualization/.env'
                  - '{{ dest_directory }}/streamonitor/.env'
                  - '{{ dest_directory }}/video_streaming/ffmpeg_live_broadcaster/.env'
                  - '{{ dest_directory }}/video_streaming/nginx_rtmp_server/.env'
                  - '{{ dest_directory }}/nginxrtmpserver/.env'
                  - '{{ dest_directory }}/restream/nginxhttpserver/.env'
                  - '{{ dest_directory }}/web_launcher/.env'
              delegate_to: boat
              run_once: true

            - name: Write hostname and IP address to .env file for land
              copy:
                  dest: '{{ dest_directory }}/.env'
                  content: |
                      HOST_IP_LAND: "{{ lookup('env', 'HOST_IP_LAND') }}"
                      HOST_IP_BOAT: "{{ lookup('env', 'HOST_IP_BOAT') }}"
                      PIXALARM_API_URL: "{{ lookup('env', 'PIXALARM_API_URL') }}"
                      MODE="{{ lookup('env', 'MODE') }}"
                  force: yes
                  mode: '0644'
              when: "'land_group' in group_names"
        tags: reload

      # =========================== K3s.yml ==================================
      - name: K3s configuration block
        block:
            - name: Check if k3s is running
              shell: systemctl is-active --quiet k3s && echo 'active' || echo 'inactive'
              register: k3s_status

            - name: Copy k3s.yaml to multiple destinations on boat
              copy:
                  src: /etc/rancher/k3s/k3s.yaml
                  dest: '{{ item }}'
                  force: yes
                  mode: '0644'
              loop:
                  - '{{ dest_directory }}/k3s.yaml'
                  - '{{ dest_directory }}/bandwidthvisualization/k3s.yaml'
                  - '{{ dest_directory }}/streamonitor/k3s.yaml'
              when: "'boat_group' in group_names"

            - name: Copy k3s.yaml to destination directory on land
              become: yes
              become_method: sudo
              command:
                  cmd: sudo cp /etc/rancher/k3s/k3s.yaml {{ dest_directory }}/k3s.yaml
              when: "'land_group' in group_names"

            - name: Change owner, group and permissions of k3s.yaml
              # become: yes
              # become_method: sudo
              shell: |
                  sudo chmod 0664 {{ dest_directory }}/k3s.yaml
                  sudo chown {{ lookup('env','LAND_SSH_USER') }}:{{ lookup('env','LAND_SSH_USER') }} {{ dest_directory }}/k3s.yaml
              when: "'land_group' in group_names"

            - name: Replace IP address in k3s.yaml for boat group
              replace:
                  path: '{{ item }}'
                  regexp: 'server: https://127.0.0.1:6443'
                  replace: 'server: https://{{ HOST_IP_BOAT }}:6443'
              loop:
                  - '{{ dest_directory }}/k3s.yaml'
                  - '{{ dest_directory }}/bandwidthvisualization/k3s.yaml'
                  - '{{ dest_directory }}/streamonitor/k3s.yaml'
              when: "'boat_group' in group_names"

            - name: Replace IP address in k3s.yaml for land group
              replace:
                  path: '{{ dest_directory }}/k3s.yaml'
                  regexp: 'server: https://127.0.0.1:6443'
                  replace: 'server: https://{{ HOST_IP_LAND }}:6443'
              when: "'land_group' in group_names"

            - name: Update HOST_IP_BOAT value in ConfigMap in classifier.yaml
              replace:
                  path: '{{ dest_directory }}/manifests/classifier.yaml'
                  regexp: 'value: ""'
                  replace: 'value: "{{ HOST_IP_BOAT }}"'
              when: "'boat_group' in group_names"

            - name: Set permissions for classifier.yaml
              file:
                  path: '{{ dest_directory }}/manifests/classifier.yaml'
                  mode: '0644'
              when: "'boat_group' in group_names"

            # - name: Update HOST_IP_BOAT value in nginx.conf in nginxhttpserver
            #   replace:
            #       path: '{{ dest_directory }}/restream/nginxhttpserver/nginx.conf'
            #       regexp: 'http://HOST_IP_BOAT:3010/hls;'
            #       replace: 'http://{{ HOST_IP_BOAT }}:3010/hls;'
            #   when: "'boat_group' in group_names"

            # - name: Set permissions for nginx.conf in nginxhttpserver
            #   file:
            #       path: '{{ dest_directory }}/restream/nginxhttpserver/nginx.conf'
            #       mode: '0644'
            #   when: "'boat_group' in group_names"



            - name: Update HOST_IP_BOAT values in HTML files
              replace:
                path: "{{ item.path }}"
                regexp: "{{ item.regexp }}"
                replace: "{{ item.replace }}"
              when: "'boat_group' in group_names"
              with_items:
                - { path: '{{ dest_directory }}/video_streaming/nginx_rtmp_server/html/dash.html', regexp: 'http://HOST_IP_BOAT:3010/dash/navidec.mpd', replace: 'http://{{ HOST_IP_BOAT }}:3010/dash/navidec.mpd' }
                - { path: '{{ dest_directory }}/video_streaming/nginx_rtmp_server/html/hls.html', regexp: 'http://HOST_IP_BOAT:3010/hls/navidec.m3u8', replace: 'http://{{ HOST_IP_BOAT }}:3010/hls/navidec.m3u8' }
                - { path: '{{ dest_directory }}/restream/nginxhttpserver/index.html', regexp: 'http://HOST_IP_BOAT:3010/hls/restream.m3u8', replace: 'http://{{ HOST_IP_BOAT }}:3010/hls/restream.m3u8' }

            - name: Set permissions for HTML files
              file:
                path: "{{ item }}"
                mode: '0644'
              when: "'boat_group' in group_names"
              with_items:
                - '{{ dest_directory }}/video_streaming/nginx_rtmp_server/html/dash.html'
                - '{{ dest_directory }}/video_streaming/nginx_rtmp_server/html/hls.html'
                - '{{ dest_directory }}/restream/nginxhttpserver/index.html'



            # - name: Update HOST_IP_BOAT value in dash.html in video_streaming
            #   replace:
            #       path: '{{ dest_directory }}/video_streaming/nginx_rtmp_server/html/dash.html'
            #       regexp: 'http://HOST_IP_BOAT/dash/navidec.mpd'
            #       replace: 'http://{{ HOST_IP_BOAT }}:3010/dash/navidec.mpd'
            #   when: "'boat_group' in group_names"

            # - name: Set permissions for dash.html in video_streaming
            #   file:
            #       path: '{{ dest_directory }}/video_streaming/nginx_rtmp_server/html/dash.html'
            #       mode: '0644'
            #   when: "'boat_group' in group_names"

            # - name: Update HOST_IP_BOAT value in hls.html in video_streaming
            #   replace:
            #       path: '{{ dest_directory }}/video_streaming/nginx_rtmp_server/html/hls.html'
            #       regexp: 'http://HOST_IP_BOAT/hls/navidec.m3u8'
            #       replace: 'http://{{ HOST_IP_BOAT }}:3010/hls/navidec.m3u8'
            #   when: "'boat_group' in group_names"

            # - name: Set permissions for hls.html in video_streaming
            #   file:
            #       path: '{{ dest_directory }}/video_streaming/nginx_rtmp_server/html/hls.html'
            #       mode: '0644'
            #   when: "'boat_group' in group_names"

            # - name: Update HOST_IP_BOAT value in index.html in nginxhttpserver
            #   replace:
            #       path: '{{ dest_directory }}/restream/nginxhttpserver/index.html'
            #       regexp: 'http://HOST_IP_BOAT:3010/hls/restream.m3u8'
            #       replace: 'http://{{ HOST_IP_BOAT }}:3010/hls/restream.m3u8'
            #   when: "'boat_group' in group_names"

            # - name: Set permissions for index.html in nginxhttpserver
            #   file:
            #       path: '{{ dest_directory }}/restream/nginxhttpserver/index.html'
            #       mode: '0644'
            #   when: "'boat_group' in group_names"
        tags: reload

      # =========================== Clean K3s deployments ==================================
      - name: Clean K8s deployments
        block:
            - name: Check if deployments exist
              shell: kubectl get deployment streaming; kubectl get deployment classifier
              register: deployment_result
              failed_when: false
              environment:
                  KUBECONFIG: /etc/rancher/k3s/k3s.yaml

            - name: Check if services exist
              shell: kubectl get service streaming-service; kubectl get service classifier-service
              register: service_result
              failed_when: false
              environment:
                  KUBECONFIG: /etc/rancher/k3s/k3s.yaml

            - name: Delete K8s deployments
              shell: |
                  kubectl delete deployment streaming
                  kubectl delete deployment classifier
              become: yes
              when: "'NotFound' not in deployment_result.stderr"
              environment:
                  KUBECONFIG: /etc/rancher/k3s/k3s.yaml

            - name: Delete K8s services
              shell: |
                  kubectl delete service streaming-service
                  kubectl delete service classifier-service
              become: yes
              when: "'NotFound' not in service_result.stderr"
              environment:
                  KUBECONFIG: /etc/rancher/k3s/k3s.yaml
        tags: reload, stop_all
      # ===========================  Copy log ==================================
      # - name: Copy log files from boat
      #   block:
      #       - name: Copy navidec_boat_logger.log from boat
      #         copy:
      #             src: /var/lib/docker/volumes/navidec_output_data/_data/navidec_boat_logger.log
      #             dest: '{{ current_dir_output.stdout }}/navidec_boat_logger.log'
      #             remote_src: no
      #             force: yes
      #             # validate_checksum: no
      #         when: "'boat_group' in group_names"
      #         become: yes
      #         ignore_errors: yes

      #       - name: Copy navidec_land_logger.log from land
      #         become: yes
      #         become_method: sudo
      #         command:
      #             cmd: sudo cp  /var/lib/docker/volumes/navidec_output_data/_data/navidec_land_logger.log '{{ current_dir_output.stdout }}/navidec_land_logger.log'
      #         when: "'land_group' in group_names"
      #         ignore_errors: yes
      #   tags: reload, stop_all
      # =========================== start docker compose ==================================
      - name: Prepare docker containers services
        block:
            - name: Docker compose down and remove images and volume
              shell: |
                  docker compose down --remove-orphans --rmi all -v
                  docker compose rm
              args:
                  chdir: '{{ dest_directory }}'
              become: yes
              ignore_errors: true

            - name: Check for running Docker containers
              shell: docker ps -a -q
              register: docker_containers
              ignore_errors: true

            - name: Stop and remove all Docker containers
              shell: |
                  docker stop {{ docker_containers.stdout_lines | join(' ') }}
                  docker rm {{ docker_containers.stdout_lines | join(' ') }}
              when: docker_containers.stdout_lines | length > 0
              become: yes
              ignore_errors: true

            - name: Remove Docker volumes
              shell: docker volume prune -f
              become: yes
              ignore_errors: true

            - name: Check for dangling Docker images
              shell: docker images -f "dangling=true" -q
              register: dangling_images
              ignore_errors: true

            - name: Remove dangling Docker images
              shell: docker rmi {{ dangling_images.stdout_lines | join(' ') }}
              when: dangling_images.stdout_lines | length > 0
              ignore_errors: true

            - name: Remove unused Docker data
              shell: docker system prune -a --volumes -f
              ignore_errors: true

            #     - name: Docker compose up
            #       command:
            #           chdir: '{{ dest_directory }}'
            #           cmd: docker compose up -d --build --force-recreate
            #       become: yes
            #       ignore_errors: true
            # when: "'boat_group' in group_names or 'land_group' in group_names"


            - name: Determine the compose override file path
              set_fact:
                  compose_override_file_path: >
                      {% if MODE == 'bandwidthvisualization' %}
                      {{ dest_directory }}/docker-compose.override.streamonitor.yml
                      {% elif MODE == 'streamonitor' %}
                      {{ dest_directory }}/docker-compose.override.bandwidthvisualization.yml
                      {% else %}
                      {{ dest_directory }}/docker-compose.override.bandwidthvisualization.yml
                      {% endif %}

            - name: Debug override file path
              debug:
                  msg: 'Checking if override file exists at {{ compose_override_file_path }}'

            - name: List directory contents for debug
              command: ls -l {{ dest_directory }}
              register: dir_listing

            - name: Debug directory contents
              debug:
                  var: dir_listing.stdout_lines

            - name: Check if the compose override file exists
              stat:
                  path: '{{ compose_override_file_path | trim }}'
              register: compose_override_file

            - name: Debug override file existence
              debug:
                  msg: 'Override file exists: {{ compose_override_file.stat.exists }}'

            - name: Check file permissions
              command: stat -c "%a %n" {{ compose_override_file_path | trim }}
              register: file_permissions
              ignore_errors: yes
              when: compose_override_file.stat.exists

            - name: Debug file permissions
              debug:
                  var: file_permissions.stdout_lines

            - name: Set compose file options
              set_fact:
                  compose_file_options: >
                      {% if compose_override_file.stat.exists %}
                      -f docker-compose.yml -f {{ compose_override_file_path | trim }}
                      {% else %}
                      -f docker-compose.yml
                      {% endif %}

            - name: Debug compose file options
              debug:
                  msg: 'Compose file options set to: {{ compose_file_options }}'

            - name: Docker compose up (boat and land)
              command: >
                  docker compose {{ compose_file_options }} up -d --build --force-recreate
              environment:
                  MODE: "{{ lookup('env', 'MODE') }}"
              args:
                  chdir: '{{ dest_directory }}'
              become: yes
              ignore_errors: true
              when: "'boat_group' in group_names or 'land_group' in group_names"
        tags: reload


            # - name: Docker compose up (boat and land)
            #   command: >
            #     docker compose.{{ MODE }} up -f -d --build --force-recreate
            #   environment:
            #     MODE: "{{ lookup('env', 'MODE') }}"
            #   args:
            #     chdir: '{{ dest_directory }}'
            #   become: yes
            #   ignore_errors: true
            #   when: "'boat_group' in group_names or 'land_group' in group_names"

      # =========================== stop all docker compose containers ===================
      - name: Stop all docker compose containers
        block:
            - name: Docker compose down and remove images and volume
              shell: |
                  docker compose down --remove-orphans --rmi all
                  docker compose rm
              args:
                  chdir: '{{ dest_directory }}'
              become: yes
              ignore_errors: true
        when: "'boat_group' in group_names or 'land_group' in group_names"
        tags: stop_all
