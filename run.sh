#!/bin/bash
# run.sh

# ========================
# IRISA Platform License
# ========================

# Copyright (c) [2024] [IRISA, Université de Rennes]

# This file is part of IRISA Platform.

# IRISA Platform is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, Version 3 of the License.

# IRISA Platform is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with IRISA Platform. If not, see <https://www.gnu.org/licenses/>.

# Define the BRANCHNAME variable
BRANCHNAME="ansible"

print_exclamation_marks() {
    printf -v exclamation_marks '%*s' 135 ''
    echo "${exclamation_marks// /!}"
}

cleanup_and_init() {
    print_exclamation_marks
    echo "Docker and Directory Cleanup with Git Repository Initialization..."

    if [ "$1" != "d" ] && [ "$1" != "down" ]; then
        sudo rm -rf "navidec" || { echo "Failed to delete navidec."; exit 1; }
        parent_dir=$(pwd)
        clone_and_copy_files "$parent_dir"
        clear
    fi

    echo "DEBUG: Running playbook based on action: $1"
    run_playbook "$1" "$parent_dir"
}

clone_and_copy_files() {
    parent_dir=$1
    git clone --branch $BRANCHNAME --single-branch https://gitlab.inria.fr/muhammad.al-qabbani/navidec.git "navidec" || { echo "Failed to clone repository."; exit 1; }

    items_to_copy=(
        ".env"
        "inventory.ini"
        "ansible.cfg"
        "run.yml"
        "setup.yml"
        "start.yml"
        "logsStreamClassifier.sh"
        "linode_instances.tf"
        ".terraform.lock.hcl"
        ".terraform"
        "terraform.tfvars"
    )

    for item in "${items_to_copy[@]}"; do
        [ -e "navidec/$item" ] && cp -rf "navidec/$item" "$parent_dir"
    done

    source "$parent_dir/.env"
    sudo rm -rf "$parent_dir/navidec"
}

run_playbook() {
    action=$1
    parent_dir=$2
    case "$action" in
        fresh | f)
            ansible-playbook -i "inventory.ini" "run.yml"
            relaunch_browser_message
            ;;
        setup | s)
            ansible-playbook -i "inventory.ini" "setup.yml" --tags "setup"
            setup_finished_message
            ;;
        "setup raspberry" | sr)
            ansible-playbook -i "inventory.ini" "raspberry.yml" --tags "setup"
            setup_finished_message
            ;;
        setupvpn | sovpn)
            ansible-playbook -i "inventory.ini" "setup.yml" --tags "setup,openvpn"
            setup_finished_message
            ;;
        "setupvpn raspberry" | sovpnr)
            ansible-playbook -i "inventory.ini" "raspberry.yml" --tags "setup,openvpn"
            setup_finished_message
            ;;
        down | d)
            source ".env"
            ansible-playbook -i "inventory.ini" "start.yml" --tags "stop_all"
            stop_finished_message
            ;;
        *)
            ansible-playbook -i "inventory.ini" "start.yml" --tags "reload"
            relaunch_browser_message
            ;;
    esac
}

setup_finished_message() {
    print_exclamation_marks
    echo "Setup finished, To launch platform, execute: ./run.sh"
    print_exclamation_marks
}

stop_finished_message() {
    print_exclamation_marks
    echo "Platform stopped, To relaunch it, execute: ./run.sh"
    print_exclamation_marks
}

relaunch_browser_message() {
    print_exclamation_marks
    cat << EOF
To relaunch browser via RPC, execute: ./run.sh web
Or to launch manually, open the browser for the following links:
${HOST_IP_BOAT}:3001
${HOST_IP_BOAT}:3002
${HOST_IP_BOAT}:3003
${HOST_IP_BOAT}:30080
${HOST_IP_BOAT}:5000/docs
${HOST_IP_LAND}:5000/docs
If the browser does not launch, execute: ./run.sh log
EOF
    print_exclamation_marks
}

docker_compose_web() {
    cd navidec
    docker exec web_launcher python3 -u web_launcher.py
    exit 0
}

docker_compose_logs_web_launcher() {
    cd navidec
    docker compose logs web_launcher
    exit 0
}

docker_compose_logs_bandwidthvisualization() {
    cd navidec
    docker compose logs bandwidthvisualization
    exit 0
}

docker_actions() {
    action=$1
    case "$action" in
        web | w)
            docker_compose_web
            exit 0
            ;;
        log | l)
            docker_compose_logs_web_launcher
            exit 0
            ;;
        bw | b)
            docker_compose_logs_bandwidthvisualization
            exit 0
            ;;
    esac
}

main() {

    # Check if the script is being executed from inside the navidec folder
    if [[ $(pwd) == *"/navidec" ]]; then
        # Code to execute if the script is being run from inside the navidec folder

        echo "DEBUG: Script is being run from inside the navidec folder"
        
        # Copy the run.sh file from the current directory to the parent directory, overwriting any existing file
        cp -f run.sh ..

        # Save the current directory path
        current_dir=$(pwd)

        # Change directory to the parent directory of navidec
        cd ..

        # Save the parent directory path
        parent_dir=$(pwd)

        docker_actions "$1"

        cleanup_and_init "$1"

    else
        docker_actions "$1"
        # Code to execute if the script is being run from outside the navidec folder
        cleanup_and_init "$1"
    fi
}

main "$1"