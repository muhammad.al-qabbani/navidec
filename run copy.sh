#!/bin/bash
# run.sh

# ========================
# IRISA Platform License
# ========================

# Copyright (c) [2024] [IRISA, Université de Rennes]

# This file is part of IRISA Platform.

# IRISA Platform is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, Version 3 of the License.

# IRISA Platform is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with IRISA Platform. If not, see <https://www.gnu.org/licenses/>.

# Define the BRANCHNAME variable
BRANCHNAME="ansible"

print_exclamation_marks() {
    for ((i=0; i<135; i++)); do
        printf "!"
    done
    echo  # This adds a newline after the exclamation marks
}


# =============================== Docker compose section =================================
# Check if the first argument is `down`
if [ "$1" = "down" ]; then
    cd navidec
    docker compose down
    exit 0
fi

# Check if the first argument is `up`
if [ "$1" = "up" ]; then
    cd navidec
    docker compose up
    exit 0
fi


# Check if the first argument is `web`
if [ "$1" = "web" ]; then
    cd navidec
    docker exec web_launcher python3 -u web_launcher.py
    exit 0
fi

# =============================== logs section =================================
# Check if the first argument is `log`
if [ "$1" = "log" ]; then
    cd navidec
    docker compose logs web_launcher
    exit 0
fi

# Check if the first argument is `bw` for bandwidth logs
if [ "$1" = "bw" ]; then
    cd navidec
    docker compose logs bandwidthvisualization
    exit 0
fi


cleanup_and_init() {

    # Remove the navidec directory and its contents
    print_exclamation_marks
    echo "Docker and Directory Cleanup with Git Repository Initialization..."
    sudo rm -rf "navidec"

    # Check if the deletion was successful
    if [ $? -eq 0 ]; then
        echo "navidec deleted successfully."
    else
        echo "Failed to delete navidec."
        exit 1
    fi

    # Define and save current working directory
    parent_dir=$(pwd)

    # Always clone the repository and navigate to the navidec folder
    git clone --branch $BRANCHNAME --single-branch https://gitlab.inria.fr/muhammad.al-qabbani/navidec.git

    # Copy files from navidec to the parent directory and overwrite existing files without asking
    if [ -f navidec/.env ]; then
        cp -f navidec/.env $parent_dir
    fi

    if [ -f navidec/inventory.ini ]; then
        cp -f navidec/inventory.ini $parent_dir
    fi

    if [ -f navidec/ansible.cfg ]; then
        cp -f navidec/ansible.cfg $parent_dir
    fi

    if [ -f navidec/setup.yml ]; then
        cp -f navidec/setup.yml $parent_dir
    fi

    if [ -f navidec/start.yml ]; then
        cp -f navidec/start.yml $parent_dir
    fi

    # if [ -f navidec/run.yml ]; then
    #     cp -f navidec/run.yml $parent_dir
    # fi
    
    if [ -f navidec/linode_instances.tf ]; then
        cp -f navidec/linode_instances.tf $parent_dir
    fi

    if [ -f navidec/.terraform.lock.hcl ]; then
        cp -f navidec/.terraform.lock.hcl $parent_dir
    fi

    if [ -d navidec/.terraform ]; then
        cp -rf navidec/.terraform $parent_dir
    fi

    if [ -d navidec/terraform.tfvars ]; then
        cp -rf navidec/terraform.tfvars $parent_dir
    fi

    # Load variable to environment
    source .env

    # Remove the navidec directory
    sudo rm -rf "navidec"

    clear


    # Determine which playbook to run based on the script argument
    echo "DEBUG: Running playbook based on action: $1"
    if [ "$1" = "setup" ] || [ "$1" = "s" ]; then
        echo "DEBUG: Running setup.yml playbook"
        ansible-playbook -i "inventory.ini" setup.yml
    elif [ "$1" = "down" ] || [ "$1" = "d" ]; then
        echo "DEBUG: Running start.yml playbook with stop_all tag"
        ansible-playbook -i "inventory.ini" start.yml --tags "stop_all"
    else
        echo "DEBUG: Running start.yml playbook"
        ansible-playbook -i "inventory.ini" start.yml
    fi


    # echo "DEBUG: Running playbook based on action: $action"
    # # Determine which playbook to run based on the script argument
    # if [ "$action" = "setup" ] || [ "$action" = "s" ]; then
    #     echo "DEBUG: Running setup.yml playbook"
    #     ansible-playbook -i "inventory.ini" setup.yml
    # else
    #     echo "DEBUG: Running run.yml playbook"
    #     ansible-playbook -i "inventory.ini" start.yml
    # fi


    # # Run ansible playbook
    # ansible-playbook -i "inventory.ini" run.yml

    # change directory to navidec
    cd "$parent_dir/navidec"
    # cd "$parent_dir"
    # cd ..

    # Save the current working directory
    current_dir=$(pwd)
    print_exclamation_marks
    echo "To relaunch browser via RPC, execute: ./run.sh web"
    echo "Or to laaunch manualy, open the browser for the following links"
    echo "${HOST_IP_BOAT}:3001"
    echo "${HOST_IP_BOAT}:3002"
    echo "${HOST_IP_BOAT}:3003"
    echo "${HOST_IP_BOAT}:30080"
    echo "${HOST_IP_BOAT}:5000/docs"
    echo "${HOST_IP_LAND}:5000/docs"
    print_exclamation_marks
    echo "If the browser does not launch, execute: cd ~ && ./run.sh log"
    print_exclamation_marks
}

reload() {
# Remove the navidec directory and its contents
    print_exclamation_marks
    echo "Redownload Navidec directore and reload Docker compose..."
    sudo rm -rf "navidec"

    # Check if the deletion was successful
    if [ $? -eq 0 ]; then
        echo "navidec deleted successfully."
    else
        echo "Failed to delete navidec."
        exit 1
    fi

    # Define and save current working directory
    parent_dir=$(pwd)

    # Always clone the repository and navigate to the navidec folder
    git clone --branch $BRANCHNAME --single-branch https://gitlab.inria.fr/muhammad.al-qabbani/navidec.git

    # Copy files from navidec to the parent directory and overwrite existing files without asking
    if [ -f navidec/inventory.ini ]; then
        cp -f navidec/inventory.ini $parent_dir
    fi

    if [ -f navidec/ansible.cfg ]; then
        cp -f navidec/ansible.cfg $parent_dir
    fi

    if [ -f navidec/setup.yml ]; then
        cp -f navidec/setup.yml $parent_dir
    fi

    if [ -f navidec/start.yml ]; then
        cp -f navidec/start.yml $parent_dir
    fi

    # if [ -f navidec/run.yml ]; then
    #     cp -f navidec/run.yml $parent_dir
    # fi
    
    if [ -f navidec/linode_instances.tf ]; then
        cp -f navidec/linode_instances.tf $parent_dir
    fi

    if [ -f navidec/.terraform.lock.hcl ]; then
        cp -f navidec/.terraform.lock.hcl $parent_dir
    fi

    if [ -d navidec/.terraform ]; then
        cp -rf navidec/.terraform $parent_dir
    fi

    if [ -d navidec/terraform.tfvars ]; then
        cp -rf navidec/terraform.tfvars $parent_dir
    fi

    # Load variable to environment
    source .env

    # Remove the navidec directory
    sudo rm -rf "navidec"

    clear

    # Run ansible playbook
    ansible-playbook -i "inventory.ini" start.yml --tags "reload"

    change directory to navidec
    cd "$parent_dir/navidec"

    # Save the current working directory
    current_dir=$(pwd)
    print_exclamation_marks
    echo "To relaunch browser via RPC, execute: ./run.sh web"
    echo "Or to laaunch manualy, open the browser for the following links"
    echo "${HOST_IP_BOAT}:3001"
    echo "${HOST_IP_BOAT}:3002"
    echo "${HOST_IP_BOAT}:3003"
    echo "${HOST_IP_BOAT}:30080"
    echo "${HOST_IP_BOAT}:5000/docs"
    echo "${HOST_IP_LAND}:5000/docs"
    print_exclamation_marks
    echo "If the browser does not launch, execute: cd ~ && ./run.sh log"
    print_exclamation_marks
}

# Main logic
# action=$1
# echo "DEBUG: Action received: $action"

# Check if the first argument is `r`
if [ "$1" = "r" ]; then
    reload

# # Check if the first argument is `r`
# if [ "$1" = "r" ]; then
#     reload
else
    # Check if the script is being executed from inside the navidec folder
    if [[ $(pwd) == *"/navidec" ]]; then
        # Code to execute if the script is being run from inside the navidec folder

        echo "DEBUG: Script is being run from inside the navidec folder"
        
        # Copy the run.sh file from the current directory to the parent directory, overwriting any existing file
        cp -f run.sh ..

        # Save the current directory path
        current_dir=$(pwd)

        # Change directory to the parent directory of navidec
        cd ..

        # Save the parent directory path
        parent_dir=$(pwd)

        # cleanup_and_init "$action"
        cleanup_and_init "$1"

    else
        # Code to execute if the script is being run from outside the navidec folder
        # cleanup_and_init "$action"
        cleanup_and_init "$1"
    fi
fi