# ansible/deploy_land.yml

# ========================
# IRISA Platform License
# ========================

# Copyright (c) [2024] [IRISA, Université de Rennes]

# This file is part of IRISA Platform.

# IRISA Platform is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, Version 3 of the License.

# IRISA Platform is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with IRISA Platform. If not, see <https://www.gnu.org/licenses/>.

---
- hosts: all
  any_errors_fatal: true
  gather_facts: no
  vars:
      ansible_host: "{{ lookup('env', 'HOST_IP_LAND') }}"
      ansible_ssh_user: "{{ lookup('env','LAND_SSH_USER') }}"
      ansible_ssh_pass: "{{ lookup('env','LAND_SSH_PASS') }}"
      ansible_ssh_extra_args: '-o StrictHostKeyChecking=no'

      HOST_IP_BOAT: "{{ lookup('env', 'HOST_IP_BOAT') }}"
      HOST_IP_LAND: "{{ lookup('env', 'HOST_IP_LAND') }}"
      PIXALARM_API_URL: "{{ lookup('env', 'PIXALARM_API_URL') }}"
      LAND_SSH_USER: "{{ lookup('env','LAND_SSH_USER') }}"
      LAND_SSH_PASS: "{{ lookup('env','LAND_SSH_PASS') }}"
      HOST_IP_RPC: "{{ lookup('env', 'HOST_IP_RPC') }}"

      openvpn_admin_username: 'openvpn'
      openvpn_admin_password: 'IRISA@2030!'
      openvpn_user1_username: 'work'
      openvpn_user1_password: 'IRISA@2030!'
      openvpn_user1_ip: '10.10.10.10'
      openvpn_user2_username: 'works'
      openvpn_user2_password: 'IRISA@2030!'
      openvpn_user2_ip: '10.10.10.20'
  tasks:
      - name: Test connectivity
        block:
            - name: Ensure sshpass is installed (setup mode)
              become: yes
              package:
                  name: sshpass
                  state: present
              when: "'boat_group' in group_names"

            - name: Ping all hosts
              ping:
              register: result
              ignore_errors: false

            - name: Fail if any host is unreachable
              fail:
                  msg: 'Host is unreachable. Exiting because not all hosts are reachable.'
              when: result is failed

            - name: Gathering Facts
              setup:
              ignore_errors: false
        tags: setup

      - name: Setup Current Directory and Destination Directory
        block:
            - name: Get Current Directory
              command: pwd
              register: current_dir_output
              changed_when: false
              ignore_errors: true

            - name: Debug Current Directory
              debug:
                  var: current_dir_output.stdout

            - name: Set Destination Directory
              set_fact:
                  dest_directory: '{{ current_dir_output.stdout }}/navidec'
              changed_when: false

            - name: Debug Destination Directory
              debug:
                  var: dest_directory

            - name: Add alias 'cls' for 'clear' command
              ansible.builtin.lineinfile:
                  path: '{{ current_dir_output.stdout }}/.bashrc'
                  line: 'alias cls="clear"'
                  create: yes
              become: yes

            - name: Add alias 'd' for 'docker' command
              ansible.builtin.lineinfile:
                  path: '{{ current_dir_output.stdout }}/.bashrc'
                  line: 'alias d="docker"'
                  create: yes
              become: yes

            - name: Add alias 'k' for 'kubectl' command
              ansible.builtin.lineinfile:
                  path: '{{ current_dir_output.stdout }}/.bashrc'
                  line: 'alias k="kubectl"'
                  create: yes
              become: yes
        tags: setup

      # =========================== Docker ==================================
      - name: Ensure git and Docker are installed
        block:
            - name: Update package repositories
              apt:
                  update_cache: yes
              become: "{{ 'local' not in ansible_connection }}"
              become_user: "{{ lookup('env', 'LAND_SSH_USER') }}"
              when: ansible_os_family == "Debian"

            - name: Install git
              package:
                  name: git
                  state: present

            - name: Check if Docker is installed
              shell: command -v docker
              register: docker_installed
              failed_when: docker_installed.rc != 0 and 'command not found' in docker_installed.stderr
              ignore_errors: true

            - name: Display message if Docker is installed
              debug:
                  msg: 'Docker is already installed'
              when: docker_installed.rc == 0

            - name: Download Docker installation script
              get_url:
                  url: https://get.docker.com
                  dest: /tmp/get-docker.sh
                  mode: '0755'
              when: ansible_os_family == "Debian" and docker_installed.rc != 0

            - name: Run Docker installation script
              command: sh /tmp/get-docker.sh
              when: ansible_os_family == "Debian" and docker_installed.rc != 0

            - name: Check if current user is in Docker group
              shell: id -nG "{{ ansible_env.USER }}" | grep -qw docker
              register: user_in_docker_group
              failed_when: user_in_docker_group.rc != 0 and 'docker' in user_in_docker_group.stdout

            - name: Display message if current user is in Docker group
              debug:
                  msg: 'Current user is already in Docker group'
              when: user_in_docker_group.rc == 0

            - name: Display message if current user is not in Docker group
              debug:
                  msg: 'Current user is not in Docker group'
              when: user_in_docker_group.rc != 0

            - name: Add current user to Docker group
              user:
                  name: '{{ ansible_env.USER }}'
                  groups: docker
                  append: yes
              when: user_in_docker_group.rc != 0

            - name: Instruct user to switch to new Docker group
              debug:
                  msg: 'Please open a new terminal to activate the Docker group'
              when: user_in_docker_group.rc != 0
        tags: setup

      # =========================== Python3 ==================================

      - name: Setup Python3 and Docker SDK
        block:
            - name: Ensure Python3 is installed
              raw: which python3 || apt-get install -y python3
              become: yes
              register: python3_installed

            - name: Set Python interpreter to Python3
              set_fact:
                  ansible_python_interpreter: /usr/bin/python3
              when: python3_installed.rc == 0

            - name: Ensure pip3 is installed
              apt:
                  name: python3-pip
                  state: present
              when: ansible_os_family == "Debian"

            - name: Check if Docker SDK for Python is installed
              shell: python3 -c "import docker"
              register: docker_sdk_installed
              ignore_errors: true

            - name: Ensure Docker SDK for Python is installed
              pip:
                  name: docker
                  state: present
                  executable: pip3
              when: docker_sdk_installed.rc != 0

        tags: setup

      # =========================== K3S ==================================
      - name: Check and install k3s
        block:
            - name: Check if k3s is running
              shell: systemctl is-active --quiet k3s && echo 'active' || echo 'inactive'
              register: k3s_status
              failed_when: k3s_status.rc != 0 and 'inactive' not in k3s_status.stdout
              ignore_errors: true

            - name: Display k3s status
              debug:
                  msg: "{{ 'k3s is running' if k3s_status.stdout == 'active' else 'k3s is not running' }}"

            - name: Install k3s on boat if not running
              shell: >
                  {% set target_ip = HOST_IP_BOAT %}
                  {% set INSTALL_K3S_EXEC = "--tls-san " + target_ip %}
                  curl -sfL https://get.k3s.io | INSTALL_K3S_EXEC="${{ INSTALL_K3S_EXEC }}" sh -
              args:
                  warn: false
              when: k3s_status.stdout != 'active' and inventory_hostname in groups['boat_group']

            - name: Install k3s on land if not running
              shell: >
                  {% set target_ip = HOST_IP_LAND %}
                  {% set INSTALL_K3S_EXEC = "--tls-san " + target_ip %}
                  curl -sfL https://get.k3s.io | INSTALL_K3S_EXEC="${{ INSTALL_K3S_EXEC }}" sh -
              args:
                  warn: false
              when: k3s_status.stdout != 'active' and inventory_hostname in groups['land_group']
        tags: setup

      # =========================== OPENVPN ==================================
      - name: Ensure openvpnas is installed
        block:
            - name: Gather package facts
              package_facts:
                  manager: auto

            - name: Check if openvpn or openvpnas is installed
              set_fact:
                  vpn_installed: "{{ 'openvpn' in ansible_facts.packages or 'openvpnas' in ansible_facts.packages }}"

            - name: Display message if openvpn or openvpnas is installed
              debug:
                  msg: 'openvpn or openvpnas is already installed'
              when: vpn_installed

            - name: Display message if openvpn or openvpnas is not installed
              debug:
                  msg: 'openvpn or openvpnas is not installed'
              when: not vpn_installed
        when: "'boat_group' in group_names"
        tags: setup

      - name: Install OpenVPN
        become: yes
        block:
            - name: Update packages
              apt:
                  update_cache: yes

            - name: Install required packages
              apt:
                  name:
                      - ca-certificates
                      - wget
                      - curl
                      - net-tools
                      - gnupg
                  state: present

            - name: Add OpenVPN repository key
              become: yes
              get_url:
                  url: https://as-repository.openvpn.net/as-repo-public.asc
                  dest: /etc/apt/trusted.gpg.d/as-repository.asc

            - name: Add OpenVPN repository for Ubuntu 22, 64 bits
              become: yes
              copy:
                  content: 'deb [arch=amd64 signed-by=/etc/apt/trusted.gpg.d/as-repository.asc] http://as-repository.openvpn.net/as/debian jammy main'
                  dest: /etc/apt/sources.list.d/openvpn-as-repo.list
              when: ansible_distribution == "Ubuntu" and ansible_distribution_version.startswith('22') and ansible_architecture == 'x86_64'

            - name: Add OpenVPN repository for Ubuntu 22, ARM64
              become: yes
              copy:
                  content: 'deb [arch=arm64 signed-by=/etc/apt/trusted.gpg.d/as-repository.asc] http://as-repository.openvpn.net/as/debian jammy main'
                  dest: /etc/apt/sources.list.d/openvpn-as-repo.list
              when: ansible_distribution == "Ubuntu" and ansible_distribution_version.startswith('22') and ansible_architecture == 'aarch64'

            - name: Add OpenVPN repository for Ubuntu 20, 64 bits
              become: yes
              copy:
                  content: 'deb [arch=amd64 signed-by=/etc/apt/trusted.gpg.d/as-repository.asc] http://as-repository.openvpn.net/as/debian focal main'
                  dest: /etc/apt/sources.list.d/openvpn-as-repo.list
              when: ansible_distribution == "Ubuntu" and ansible_distribution_version.startswith('20') and ansible_architecture == 'x86_64'

            - name: Add OpenVPN repository for Ubuntu 20, ARM64
              become: yes
              copy:
                  content: 'deb [arch=arm64 signed-by=/etc/apt/trusted.gpg.d/as-repository.asc] http://as-repository.openvpn.net/as/debian focal main'
                  dest: /etc/apt/sources.list.d/openvpn-as-repo.list
              when: ansible_distribution == "Ubuntu" and ansible_distribution_version.startswith('20') and ansible_architecture == 'aarch64'

            - name: Add OpenVPN repository for Debian 10, 64 bits
              become: yes
              copy:
                  content: 'deb [arch=amd64 signed-by=/etc/apt/trusted.gpg.d/as-repository.asc] http://as-repository.openvpn.net/as/debian buster main'
                  dest: /etc/apt/sources.list.d/openvpn-as-repo.list
              when: ansible_distribution == "Debian" and ansible_distribution_version.startswith('10') and ansible_architecture == 'x86_64'

            - name: Add OpenVPN repository for Debian 11, 64 bits
              become: yes
              copy:
                  content: 'deb [arch=amd64 signed-by=/etc/apt/trusted.gpg.d/as-repository.asc] http://as-repository.openvpn.net/as/debian bullseye main'
                  dest: /etc/apt/sources.list.d/openvpn-as-repo.list
              when: ansible_distribution == "Debian" and ansible_distribution_version.startswith('11') and ansible_architecture == 'x86_64'

            - name: Add OpenVPN repository for Debian 12, 64 bits
              become: yes
              copy:
                  content: 'deb [arch=amd64 signed-by=/etc/apt/trusted.gpg.d/as-repository.asc] http://as-repository.openvpn.net/as/debian bookworm main'
                  dest: /etc/apt/sources.list.d/openvpn-as-repo.list
              when: ansible_distribution == "Debian" and ansible_distribution_version.startswith('12') and ansible_architecture == 'x86_64'

            - name: Update package lists after adding new repository
              become: yes
              apt:
                  update_cache: yes

            - name: Install OpenVPN
              become: yes
              command: apt -y install openvpn-as
              register: install_output

            - name: Display installation output
              become: yes
              debug:
                  var: install_output.stdout_lines

            - name: Set hostname IP
              become: yes
              command: "/usr/local/openvpn_as/scripts/sacli --key 'host.name' --value '{{ HOST_IP_BOAT }}' ConfigPut"
              register: result_net

            - name: Set vpn.server.group_pool
              become: yes
              command: /usr/local/openvpn_as/scripts/sacli --key "vpn.server.group_pool.0" --value "172.0.0.0/8" ConfigPut

            - name: Set tcp.n_daemons
              become: yes
              command: /usr/local/openvpn_as/scripts/sacli -k vpn.server.daemon.tcp.n_daemons -v 4 configput

            - name: Set udp.n_daemons
              become: yes
              command: /usr/local/openvpn_as/scripts/sacli -k vpn.server.daemon.udp.n_daemons -v 4 configput

            - name: Set static network
              become: yes
              command: /usr/local/openvpn_as/scripts/sacli --key "vpn.server.static.0.network" --value "10.10.0.0" ConfigPut
              register: result_net

            - name: Set netmask bits
              become: yes
              command: /usr/local/openvpn_as/scripts/sacli --key "vpn.server.static.0.netmask_bits" --value "16" ConfigPut
              register: result_bits

            - name: Set private subnet
              become: yes
              command: /usr/local/openvpn_as/scripts/sacli --key "vpn.server.routing.private_access" --value "no" ConfigPut
              register: result_private_subnet

            - name: Restart OpenVPN Access Server
              become: yes
              command: /usr/local/openvpn_as/scripts/sacli start
              when: result_net.rc == 0 and result_bits.rc == 0

            - name: Set client Internet traffic be routed through the VPN as false
              become: yes
              command: /usr/local/openvpn_as/scripts/sacli --key "vpn.client.routing.reroute_gw" --value "false" ConfigPut

            - name: Do not alter clients' DNS server settings
              become: yes
              command: /usr/local/openvpn_as/scripts/sacli --key "vpn.client.routing.reroute_dns" --value "false" ConfigPut

            - name: Restart OpenVPN Access Server
              become: yes
              command: /usr/local/openvpn_as/scripts/sacli start

            - name: Set autologin for user "{{ openvpn_user1_username }}"
              become: yes
              command: /usr/local/openvpn_as/scripts/sacli --user "{{ openvpn_user1_username }}" --key "type" --value "user_connect" --key "prop_autologin" --value "true" UserPropPut

            - name: Change the password for user "{{ openvpn_user1_username }}"
              become: yes
              command: /usr/local/openvpn_as/scripts/sacli --user "{{ openvpn_user1_username }}" --new_pass "{{ openvpn_user1_password }}" SetLocalPassword

            - name: Set connection IP for user "{{ openvpn_user1_username }}"
              become: yes
              command: /usr/local/openvpn_as/scripts/sacli --user "{{ openvpn_user1_username }}" --key "conn_ip" --value {{ openvpn_user1_ip }} UserPropPut

            - name: Restart OpenVPN Access Server
              become: yes
              command: /usr/local/openvpn_as/scripts/sacli start

            - name: Set autologin for user "{{ openvpn_user2_username }}"
              become: yes
              command: /usr/local/openvpn_as/scripts/sacli --user "{{ openvpn_user2_username }}" --key "type" --value "user_connect" --key "prop_autologin" --value "true" UserPropPut

            - name: Change the password for user "{{ openvpn_user2_username }}"
              become: yes
              command: /usr/local/openvpn_as/scripts/sacli --user "{{ openvpn_user2_username }}" --new_pass "{{ openvpn_user1_password }}" SetLocalPassword

            - name: Set connection IP for user "{{ openvpn_user2_username }}"
              become: yes
              command: /usr/local/openvpn_as/scripts/sacli --user "{{ openvpn_user2_username }}" --key "conn_ip" --value {{ openvpn_user2_ip }} UserPropPut

            - name: Restart OpenVPN Access Server
              become: yes
              command: /usr/local/openvpn_as/scripts/sacli start

            - name: Set user as a superuser
              become: yes
              command: /usr/local/openvpn_as/scripts/sacli --user "{{ openvpn_admin_username }}" --key "prop_superuser" --value "true" UserPropPut

            - name: Set user authentication type to local
              become: yes
              command: /usr/local/openvpn_as/scripts/sacli --user "{{ openvpn_admin_username }}" --key "user_auth_type" --value "local" UserPropPut

            - name: Change the password for the OpenVPN admin user
              become: yes
              command: /usr/local/openvpn_as/scripts/sacli --user "{{ openvpn_admin_username }}" --new_pass "{{ openvpn_admin_password }}" SetLocalPassword

            - name: Start OpenVPN Access Server
              become: yes
              command: /usr/local/openvpn_as/scripts/sacli start

            - name: Display OpenVPN admin username and password
              become: yes
              debug:
                  msg: 'OpenVPN Admin Username: {{ openvpn_admin_username }}, OpenVPN Admin Password: {{ openvpn_admin_password }}'

            - name: Display OpenVPN username and password for user 1
              become: yes
              debug:
                  msg: 'OpenVPN Username: {{ openvpn_user1_username }}, OpenVPN Password: {{ openvpn_user1_password }}'

            - name: Display OpenVPN username and password for user 2
              become: yes
              debug:
                  msg: 'OpenVPN Username: {{ openvpn_user2_username }}, OpenVPN Password: {{ openvpn_user2_password }}'

            - name: Display Admin and Client UI URLs
              become: yes
              debug:
                  msg:
                      - 'openvpn Access Server Web UIs are available here:'
                      - 'Admin UI: https://{{ HOST_IP_BOAT }}:943/admin'
                      - 'Client UI: https://{{ HOST_IP_BOAT }}:943/'
        when: "'boat_group' in group_names and not vpn_installed"
        tags: openvpn  # Only run when 'openvpn' tag is used
