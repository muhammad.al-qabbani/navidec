#!/bin/bash
# run.sh

# ========================
# IRISA Platform License
# ========================

# Copyright (c) [2024] [IRISA, Université de Rennes]

# This file is part of IRISA Platform.

# IRISA Platform is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, Version 3 of the License.

# IRISA Platform is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with IRISA Platform. If not, see <https://www.gnu.org/licenses/>.

# Define the BRANCHNAME variable
BRANCHNAME="ansible"

print_exclamation_marks() {
    printf -v exclamation_marks '%*s' 135 ''
    echo "${exclamation_marks// /!}"
}

clone_and_copy_files() {
    git clone --branch $BRANCHNAME --single-branch https://gitlab.inria.fr/muhammad.al-qabbani/navidec.git

    items_to_copy=(
        ".env"
        "inventory.ini"
        "ansible.cfg"
        "setup.yml"
        "start.yml"
        "linode_instances.tf"
        ".terraform.lock.hcl"
        ".terraform"
        "terraform.tfvars"
    )

    for item in "${items_to_copy[@]}"; do
        [ -e "navidec/$item" ] && cp -rf "navidec/$item" "$parent_dir"
    done

    source .env
    sudo rm -rf "navidec"
}

run_playbook() {
    case "$1" in
        setup | s)
            ansible-playbook -i "inventory.ini" setup.yml
            ;;
        down | d)
            ansible-playbook -i "inventory.ini" start.yml --tags "stop_all"
            ;;
        *)
            ansible-playbook -i "inventory.ini" start.yml
            ;;
    esac
}

cleanup_and_init() {

    # Remove the navidec directory and its contents
    print_exclamation_marks
    echo "Docker and Directory Cleanup with Git Repository Initialization..."
    sudo rm -rf "navidec"

    # Check if the deletion was successful
    if [ $? -eq 0 ]; then
        echo "navidec deleted successfully."
    else
        echo "Failed to delete navidec."
        exit 1
    fi

    # Define and save current working directory
    parent_dir=$(pwd)

    # # Always clone the repository and navigate to the navidec folder
    # git clone --branch $BRANCHNAME --single-branch https://gitlab.inria.fr/muhammad.al-qabbani/navidec.git

    # # Files and directories to copy
    # items_to_copy=(
    #     ".env"
    #     "inventory.ini"
    #     "ansible.cfg"
    #     "setup.yml"
    #     "start.yml"
    #     "linode_instances.tf"
    #     ".terraform.lock.hcl"
    #     ".terraform"
    #     "terraform.tfvars"
    # )

    # # Copy files and directories from navidec to the parent directory
    # for item in "${items_to_copy[@]}"; do
    #     if [ -e "navidec/$item" ]; then
    #         cp -rf "navidec/$item" "$parent_dir"
    #     fi
    # done

    # # Load variable to environment
    # source .env

    # # Remove the navidec directory
    # sudo rm -rf "navidec"

    clear


    # Determine which playbook to run based on the script argument
    echo "DEBUG: Running playbook based on action: $1"
    if [ "$1" = "setup" ] || [ "$1" = "s" ]; then
        echo "DEBUG: Running setup.yml playbook"
        ansible-playbook -i "inventory.ini" setup.yml
    elif [ "$1" = "down" ] || [ "$1" = "d" ]; then
        echo "DEBUG: Running start.yml playbook with stop_all tag"
        ansible-playbook -i "inventory.ini" start.yml --tags "stop_all"
    else
        echo "DEBUG: Running start.yml playbook"
        ansible-playbook -i "inventory.ini" start.yml
    fi


    # change directory to navidec
    cd "$parent_dir/navidec"

    # Save the current working directory
    current_dir=$(pwd)
    print_exclamation_marks
    echo "To relaunch browser via RPC, execute: ./run.sh web"
    echo "Or to laaunch manualy, open the browser for the following links"
    echo "${HOST_IP_BOAT}:3001"
    echo "${HOST_IP_BOAT}:3002"
    echo "${HOST_IP_BOAT}:3003"
    echo "${HOST_IP_BOAT}:30080"
    echo "${HOST_IP_BOAT}:5000/docs"
    echo "${HOST_IP_LAND}:5000/docs"
    print_exclamation_marks
    echo "If the browser does not launch, execute: cd ~ && ./run.sh log"
    print_exclamation_marks
}


# Check if the script is being executed from inside the navidec folder
if [[ $(pwd) == *"/navidec" ]]; then
    # Code to execute if the script is being run from inside the navidec folder

    echo "DEBUG: Script is being run from inside the navidec folder"
    
    # Copy the run.sh file from the current directory to the parent directory, overwriting any existing file
    cp -f run.sh ..

    # Save the current directory path
    current_dir=$(pwd)

    # Change directory to the parent directory of navidec
    cd ..

    # Save the parent directory path
    parent_dir=$(pwd)

    # cleanup_and_init "$action"
    cleanup_and_init "$1"

else
    # Code to execute if the script is being run from outside the navidec folder
    # cleanup_and_init "$action"
    cleanup_and_init "$1"
fi