#!/bin/bash

pod_name=$(kubectl get pods -n default -l app=classifier -o name | head -n 1)

if [ -z "$pod_name" ]; then
  echo "No pods found with label app=classifier"
else
  kubectl logs -f $pod_name -n default
fi