#!/bin/bash

# =========================== Update package repositories ===========================
echo "Updating package repositories..."
sudo apt update

# =========================== Install Git ===========================
echo "Installing Git..."
sudo apt install -y git

# =========================== Check if Docker is installed ===========================
echo "Checking if Docker is installed..."
if ! command -v docker &> /dev/null
then
    echo "Docker not found. Installing Docker..."

    # =========================== Download Docker installation script ===========================
    echo "Downloading Docker installation script..."
    curl -fsSL https://get.docker.com -o /tmp/get-docker.sh
    chmod +x /tmp/get-docker.sh

    # =========================== Run Docker installation script ===========================
    echo "Running Docker installation script..."
    sh /tmp/get-docker.sh
else
    echo "Docker is already installed."
fi

# =========================== Check if current user is in Docker group ===========================
echo "Checking if the current user is in the Docker group..."
if id -nG "$USER" | grep -qw docker; then
    echo "Current user is already in the Docker group."
else
    # =========================== Add current user to Docker group ===========================
    echo "Adding current user to Docker group..."
    sudo usermod -aG docker $USER
    echo "Please open a new terminal to activate the Docker group."
fi

# =========================== Ensure Python3 is installed ===========================
echo "Ensuring Python3 is installed..."
if ! which python3 &> /dev/null; then
    sudo apt-get install -y python3
else
    echo "Python3 is already installed."
fi

# =========================== Set Python interpreter to Python3 ===========================
echo "Setting Python3 as the interpreter..."
export ansible_python_interpreter=/usr/bin/python3

# =========================== Ensure pip3 is installed ===========================
echo "Ensuring pip3 is installed..."
sudo apt-get install -y python3-pip

# =========================== Check if Docker SDK for Python is installed ===========================
echo "Checking if Docker SDK for Python is installed..."
if ! python3 -c "import docker" &> /dev/null; then
    # =========================== Install Docker SDK for Python ===========================
    echo "Installing Docker SDK for Python..."
    pip3 install docker
else
    echo "Docker SDK for Python is already installed."
fi

