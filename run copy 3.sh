#!/bin/bash
# run.sh

# ========================
# IRISA Platform License
# ========================

# Copyright (c) [2024] [IRISA, Université de Rennes]

# This file is part of IRISA Platform.

# IRISA Platform is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, Version 3 of the License.

# IRISA Platform is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with IRISA Platform. If not, see <https://www.gnu.org/licenses/>.

# Define the BRANCHNAME variable
BRANCHNAME="ansible"

print_exclamation_marks() {
    printf -v exclamation_marks '%*s' 135 ''
    echo "${exclamation_marks// /!}"
}

cleanup_and_init() {
    print_exclamation_marks
    echo "Docker and Directory Cleanup with Git Repository Initialization..."
    sudo rm -rf "navidec" || { echo "Failed to delete navidec."; exit 1; }

    parent_dir=$(pwd)
    clone_and_copy_files
    clear

    echo "DEBUG: Running playbook based on action: $1"
    run_playbook "$1"

    cd "navidec"
}

clone_and_copy_files() {
    git clone --branch $BRANCHNAME --single-branch https://gitlab.inria.fr/muhammad.al-qabbani/navidec.git "navidec" || { echo "Failed to clone repository."; exit 1; }

    items_to_copy=(
        ".env"
        "inventory.ini"
        "ansible.cfg"
        "run.yml"
        "setup.yml"
        "start.yml"
        "linode_instances.tf"
        ".terraform.lock.hcl"
        ".terraform"
        "terraform.tfvars"
    )

    for item in "${items_to_copy[@]}"; do
        [ -e "navidec/$item" ] && cp -rf "navidec/$item" "$parent_dir"
    done

    source .env
    sudo rm -rf "navidec"
}

run_playbook() {
    case "$1" in
        setup | s)
            ansible-playbook -i "$parent_dir/inventory.ini" "$parent_dir/setup.yml"
            setup_finished_message
            ;;
        down | d)
            ansible-playbook -i "$parent_dir/inventory.ini" "$parent_dir/start.yml" --tags "stop_all"
            relaunch_browser_message
            ;;
        *)
            # ansible-playbook -i "inventory.ini" "start.yml"
            ansible-playbook -i "$parent_dir/inventory.ini" "$parent_dir/run.yml"
            relaunch_browser_message
            ;;
    esac
}

setup_finished_message() {
    print_exclamation_marks
    echo "Setup finished, To launch platform, execute: ./run.sh"
    print_exclamation_marks
}

relaunch_browser_message() {
    print_exclamation_marks
    cat << EOF
To relaunch browser via RPC, execute: ./run.sh web
Or to launch manually, open the browser for the following links:
${HOST_IP_BOAT}:3001
${HOST_IP_BOAT}:3002
${HOST_IP_BOAT}:3003
${HOST_IP_BOAT}:30080
${HOST_IP_BOAT}:5000/docs
${HOST_IP_LAND}:5000/docs
If the browser does not launch, execute: cd ~ && ./run.sh log
EOF
    print_exclamation_marks
}

main() {
    if [[ $(pwd) != *"/navidec" ]]; then
        echo "Please run this script from inside the 'navidec' directory."
        exit 1
    fi

    cleanup_and_init "$1"
}

main "$1"
