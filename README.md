# Ansible

> **Note:** The Ansible playbook tested on ubuntu 22 and 20.


**01-** Execute the following commands on 'Boat' Server side.

```bash
sudo apt-get update && sudo apt-get install git sshpass ansible -y
```

**02-** Download the repository 

```bash
rm -rf navidec && git clone --branch ansible --single-branch https://gitlab.inria.fr/muhammad.al-qabbani/navidec.git && cd navidec
```

**03-** Update the variables inside .env file

**04-** Run the Ansible playbook **Setup** for the first time to download the required packages and follow the instructions at the end of the script:

To run the setup without OpenVPN:

```bash
./run.sh setup && cd ..
```
To run the setup with OpenVPN:

```bash
./run.sh setupvpn && cd ..
```

For reference incase use setup with OpenVPN:

```

openvpn Access Server Web UIs are available here:

###### Admin UI: https://{{ HOST_IP_BOAT }}:943/admin ######

openvpn admin username: openvpn 
openvpn admin password: IRISA@2030!

###### Client UI: https://{{ HOST_IP_BOAT }}:943 ######

openvpn user1 username: work
openvpn user1 password: IRISA@2030!
openvpn user1 ip: 10.10.10.10

openvpn user2 username: works
openvpn user2 password: IRISA@2030!
openvpn user2 ip: 10.10.10.20
```

**05-** Download and launch the **[rpc_server_web_launcher.py](https://gitlab.inria.fr/muhammad.al-qabbani/navidec/-/raw/boat/web_launcher/rpc_server_web_launcher.py?ref_type=heads&inline=false)** file on your local machine, from which you intend to launch the browser. It is imperative to ensure that Python 3 is pre-installed on your local machine. Additionally, verify that the file is not being obstructed by any existing system firewall.

&nbsp;&nbsp;&nbsp;&nbsp;**5.1** Ensure that Google Chrome is already installed if your local operating system is Windows, and Firefox if your local operating system is Linux.

&nbsp;&nbsp;&nbsp;&nbsp;**5.2** If you are utilizing a Windows operating system, the following command should be executed within an elevated PowerShell environment (Run as Administrator). This action is necessary to enable the passage of incoming ICMP echo requests, which are frequently referred to as "pings".

> **Note:** The following commands first import the necessary network security module and then adjust the firewall settings to allow incoming ICMP echo requests (also known as "pings").

```powershell
# Import the necessary network security module
Import-Module NetSecurity

# Add a new rule to the advanced firewall settings
# This rule allows incoming ICMPv4 echo requests (pings)
netsh advfirewall firewall add rule name="ICMP Allow incoming V4 echo request" protocol=icmpv4:8,any dir=in action=allow
```




**06-** When Setup finished, To launch platform, execute: 

```bash
./run.sh
```


**07-** Full command summary to relaunc the platform

```bash
rm -rf navidec && git clone --branch ansible --single-branch https://gitlab.inria.fr/muhammad.al-qabbani/navidec.git && cd navidec && ./run.sh && cd ..
```

## License

Distributed under [![license GPLv3](https://img.shields.io/badge/license-GPLv3-blue)](https://gitlab.inria.fr/muhammad.al-qabbani/navidec/-/blob/ansible/LICENSE)


